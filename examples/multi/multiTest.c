#ifdef _WIN32
#include <windows.h>
#include <VideoIM.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#ifndef __APPLE__
#include <GL/gl.h>
#include <GL/glut.h>
#else
#include <OpenGL/gl.h>
#include <GLUT/glut.h>
#endif
#include <AR/gsub.h>
#include <AR/video.h>
#include <AR/param.h>
#include <AR/ar.h>
#include <AR/arMulti.h>

/* set up the video format globals */

#ifdef _WIN32
char			*vconf = "Data\\WDM_camera_flipV.xml";
#else
char			*vconf = "";
#endif

int             xsize, ysize;
int             thresh = 100;
int             count = 0;

char           *cparam_name    = "Data/camera_para.dat";
ARParam         cparam;

char                *config_name = "Data/multi/marker.dat";
ARMultiMarkerInfoT  *config;

typedef struct Deco{
    int     tipo; // 1 mesa / 2 cadeira / 3 - quadro
    int     parede; // 1 esquerda / 2 direita
    int     pos_x;
    int     pos_y;
    int     pos_z;
} Deco;

typedef struct Action{
    int     type; // 1 - undo move / 2 - undo item creation;
    int     orig_pos_x;
    int     orig_pos_y;
    int     orig_pos_z;
} Action ;

static void init(void);
static void cleanup(void);
static void keyEvent( unsigned char key, int x, int y);
static void keyArrowEvent(int key, int x, int y);
static void mainLoop(void);
static void draw( double trans1[3][4], double trans2[3][4], int mode );
static void drawLimits( double trans1[3][4], int mode );
static void drawDecoration( double trans1[3][4], Deco item , int mode);
static void drawTable();
static void drawChair();
static void drawPicture();
static void undo();

int decoCount = 0;
Deco itens[50];
int moving = -1;
Action undoAction ;
int main(int argc, char **argv)
{
	glutInit(&argc, argv);
    init();

    arVideoCapStart();
    glutSpecialFunc(keyArrowEvent);
    undoAction.type = 0;
    argMainLoop( NULL, keyEvent, mainLoop );
	return (0);
}




static void   keyEvent( unsigned char key, int x, int y)
{
    /* quit if the ESC key is pressed */
    if( key == 0x1b ) {
        printf("*** %f (frame/sec)\n", (double)count/arUtilTimer());
        cleanup();
        exit(0);
    }

    if( key == 't' ) {
        printf("*** %f (frame/sec)\n", (double)count/arUtilTimer());
        printf("Enter new threshold value (current = %d): ", thresh);
        scanf("%d",&thresh); while( getchar()!='\n' );
        printf("\n");
        printf("Escolha um item que deseja adicionar:\n");
        printf(" 1 - mesa \n 2 - cadeira \n 3 - quadro \n");
        count = 0;
    }

    /* turn on and off the debug mode with right mouse */
    if( key == 'd' ) {
        printf("*** %f (frame/sec)\n", (double)count/arUtilTimer());
        arDebug = 1 - arDebug;
        if( arDebug == 0 ) {
            glClearColor( 0.0, 0.0, 0.0, 0.0 );
            glClear(GL_COLOR_BUFFER_BIT);
            argSwapBuffers();
            glClear(GL_COLOR_BUFFER_BIT);
            argSwapBuffers();
        }
        count = 0;
    }

    // undo the last action
    if( key == 'u' ) {
        undo();
    }

    // add a new item
    if( key == 'a' || key == 'A' ) {
        int option = 0;
        while (option != 1 && option != 2 && option != 3 ){
            printf("Escolha um item que deseja adicionar:\n");
            printf(" 1 - mesa \n 2 - cadeira \n 3 - quadro \n");
            scanf("%d",&option); while( getchar()!='\n' );
            printf("\n");
            printf ("Opcao escolhida: %d", option);
            printf("\n");
            if (option != 1 && option != 2 && option != 3 ){
                printf("Invalid option \n");
            }
        }
        itens[decoCount].tipo = option;
        if (option == 3){
            option = 0;
            while (option != 1 && option != 2){
                printf("Escolha uma parede para colocar o quadro.\n");
                printf(" 1 - Esquerda \n 2 - Direita\n");
                scanf("%d",&option); while( getchar()!='\n' );
                printf("\n");
                if (option != 1 && option != 2 ){
                    printf("Invalid option \n");
                }
            }
            itens[decoCount].parede = option;
            if (option == 1 )
                itens[decoCount].pos_x = 0;
            else
                itens[decoCount].pos_x = 400;
            itens[decoCount].pos_y = -150;
            itens[decoCount].pos_z = 150;
        }else {
            itens[decoCount].pos_x = 100;
            itens[decoCount].pos_y = -50;
            itens[decoCount].pos_z = 0;
        }
        undoAction.type = 2;
        decoCount++;
        count = 0;

    }

    // add a new item
    if(key == 'm' || key == 'M') {
        int option = -1;
        int i = 0;
        while (option < 0 || option > decoCount  ){
            printf("Escolha uma mobilia que deseja mover:\n");
            printf(" 0 - Nenhuma\n");
            for(i = 0 ; i < decoCount ;i++){
                char descricao[30];
                if (itens[i].tipo == 1 )
                    strcpy(descricao,"Mesa");
                else if (itens[i].tipo == 2 )
                    strcpy(descricao,"Cadeira");
                else if (itens[i].tipo == 3 ){
                    if (itens[i].parede == 1 ){
                        strcpy(descricao,"Quadro [parede esquerda]");
                    }else{
                        strcpy(descricao,"Quadro [parede direita]");
                    }
                }
                printf(" %d - %s\n", i + 1, descricao);
            }

            scanf("%d",&option); while( getchar()!='\n' );
            printf("\n");
            printf ("Opcao escolhida: %d", option);
            printf("\n");
            if (option < 0 || option > decoCount  ){
                printf("Invalid option \n");
            }
        }
        moving = option - 1;
        if (option != 0){
                printf ("Utilize as setas para mover a mobilia selecionada e selecione a op��o \"0 - Nenhum\" para finalizar.\n");
                undoAction.type = 1;
                undoAction.orig_pos_x = itens[moving].pos_x;
                undoAction.orig_pos_y = itens[moving].pos_y;
                undoAction.orig_pos_z = itens[moving].pos_z;
        }
        count = 0;
    }
}

static void keyArrowEvent(int key, int x, int y){
    if( key == GLUT_KEY_LEFT && moving != -1){
        if (itens[moving].tipo == 3){
            if (itens[moving].parede == 1){
                if (itens[moving].pos_y > -285){
                    itens[moving].pos_y -= 5;
                }
            }else {
                if (itens[moving].pos_y < -50){
                    itens[moving].pos_y += 5;
                }
            }
        }else if (itens[moving].tipo == 1){
            if (itens[moving].pos_x > 100){
                itens[moving].pos_x -= 5;
            }
        }else if (itens[moving].tipo == 2){
            if (itens[moving].pos_x > 20){
                itens[moving].pos_x -= 5;
            }
        }
    }
    if( key == GLUT_KEY_RIGHT && moving != -1){
        if (itens[moving].tipo == 3){
            if (itens[moving].parede == 2){
                if (itens[moving].pos_y > -285){
                    itens[moving].pos_y -= 5;
                }
            }else {
                if (itens[moving].pos_y < -50){
                    itens[moving].pos_y += 5;
                }
            }
        }else if (itens[moving].tipo == 1){
            if (itens[moving].pos_x < 315){
                itens[moving].pos_x += 5;
            }
        }else if (itens[moving].tipo == 2){
            if (itens[moving].pos_x < 390){
                itens[moving].pos_x += 5;
            }
        }
    }

    if( key == GLUT_KEY_UP&& moving != -1){
        if (itens[moving].tipo == 3){
            if (itens[moving].pos_z < 225){
                itens[moving].pos_z += 5;
            }
        }else{
            if (itens[moving].pos_y < -50){
                itens[moving].pos_y += 5;
            }
        }
    }
    if( key == GLUT_KEY_DOWN && moving != -1){
        if (itens[moving].tipo == 3){
            if (itens[moving].pos_z > 25){
                itens[moving].pos_z -= 5;
            }
        }else{
            if (itens[moving].pos_y > -255){
                itens[moving].pos_y -= 5;
            }
        }
    }
}

/* main loop */
static void mainLoop(void)
{
    ARUint8         *dataPtr;
    ARMarkerInfo    *marker_info;
    int             marker_num;
    double          err;
    int             i;

    /* grab a vide frame */
    if( (dataPtr = (ARUint8 *)arVideoGetImage()) == NULL ) {
        arUtilSleep(2);
    glTranslatef(0,0,10);
        return;
    }
    if( count == 0 ) arUtilTimerReset();
    count++;

    /* detect the markers in the video frame */
    if( arDetectMarkerLite(dataPtr, thresh, &marker_info, &marker_num) < 0 ) {
        cleanup();
        exit(0);
    }

    argDrawMode2D();
    if( !arDebug ) {
        argDispImage( dataPtr, 0,0 );
    }
    else {
        argDispImage( dataPtr, 1, 1 );
        if( arImageProcMode == AR_IMAGE_PROC_IN_HALF )
            argDispHalfImage( arImage, 0, 0 );
        else
            argDispImage( arImage, 0, 0);

        glColor3f( 1.0, 0.0, 0.0 );
        glLineWidth( 1.0 );
        for( i = 0; i < marker_num; i++ ) {
            argDrawSquare( marker_info[i].vertex, 0, 0 );
        }
        glLineWidth( 1.0 );
    }

    arVideoCapNext();

    if( (err=arMultiGetTransMat(marker_info, marker_num, config)) < 0 ) {
        argSwapBuffers();
        return;
    }
    //printf("err = %f\n", err);
    if(err > 100.0 ) {
        argSwapBuffers();
        return;
    }
/*
    for(i=0;i<3;i++) {
        for(j=0;j<4;j++) printf("%10.5f ", config->trans[i][j]);
        printf("\n");
    }
    printf("\n");
*/
    argDrawMode3D();
    argDraw3dCamera( 0, 0 );
    glClearDepth( 1.0 );
    glClear(GL_DEPTH_BUFFER_BIT);
    /*for( i = 0; i < config->marker_num; i++ ) {
        if( config->marker[i].visible >= 0 ) draw( config->trans, config->marker[i].trans, 0 );
        else                                 draw( config->trans, config->marker[i].trans, 1 );
    }*/

    drawLimits(config->trans, 0 );

    argSwapBuffers();
}

static void init( void )
{
    ARParam  wparam;

    /* open the video path */
    if( arVideoOpen( vconf ) < 0 ) exit(0);
    /* find the size of the window */
    if( arVideoInqSize(&xsize, &ysize) < 0 ) exit(0);
    printf("Image size (x,y) = (%d,%d)\n", xsize, ysize);

    /* set the initial camera parameters */
    if( arParamLoad(cparam_name, 1, &wparam) < 0 ) {
        printf("Camera parameter load error !!\n");
        exit(0);
    }
    arParamChangeSize( &wparam, xsize, ysize, &cparam );
    arInitCparam( &cparam );
    printf("*** Camera Parameter ***\n");
    arParamDisp( &cparam );

    if( (config = arMultiReadConfigFile(config_name)) == NULL ) {
        printf("config data load error !!\n");
        exit(0);
    }

    /* open the graphics window */
    argInit( &cparam, 1.0, 0, 2, 1, 0 );
    arFittingMode   = AR_FITTING_TO_IDEAL;
    arImageProcMode = AR_IMAGE_PROC_IN_HALF;
    argDrawMode     = AR_DRAW_BY_TEXTURE_MAPPING;
    argTexmapMode   = AR_DRAW_TEXTURE_HALF_IMAGE;
}

/* cleanup function called when program exits */
static void cleanup(void)
{
    arVideoCapStop();
    arVideoClose();
    argCleanup();
}

static void draw( double trans1[3][4], double trans2[3][4], int mode )
{
    double    gl_para[16];
    GLfloat   mat_ambient[]     = {0.0, 0.0, 1.0, 1.0};
    GLfloat   mat_ambient1[]    = {1.0, 0.0, 0.0, 1.0};
    GLfloat   mat_flash[]       = {0.0, 0.0, 1.0, 1.0};
    GLfloat   mat_flash1[]      = {1.0, 0.0, 0.0, 1.0};
    GLfloat   mat_flash_shiny[] = {50.0};
    GLfloat   mat_flash_shiny1[]= {50.0};
    GLfloat   light_position[]  = {100.0,-200.0,200.0,0.0};
    GLfloat   ambi[]            = {0.1, 0.1, 0.1, 0.1};
    GLfloat   lightZeroColor[]  = {0.9, 0.9, 0.9, 0.1};

    argDrawMode3D();
    argDraw3dCamera( 0, 0 );
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    /* load the camera transformation matrix */
    glMatrixMode(GL_MODELVIEW);
    argConvGlpara(trans1, gl_para);
    glLoadMatrixd( gl_para );
    argConvGlpara(trans2, gl_para);
    glMultMatrixd( gl_para );

    if( mode == 0 ) {
        glEnable(GL_LIGHTING);
        glEnable(GL_LIGHT0);
        glLightfv(GL_LIGHT0, GL_POSITION, light_position);
        glLightfv(GL_LIGHT0, GL_AMBIENT, ambi);
        glLightfv(GL_LIGHT0, GL_DIFFUSE, lightZeroColor);
        glMaterialfv(GL_FRONT, GL_SPECULAR, mat_flash);
        glMaterialfv(GL_FRONT, GL_SHININESS, mat_flash_shiny);
        glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient);
    }
    else {
        glEnable(GL_LIGHTING);
        glEnable(GL_LIGHT0);
        glLightfv(GL_LIGHT0, GL_POSITION, light_position);
        glLightfv(GL_LIGHT0, GL_AMBIENT, ambi);
        glLightfv(GL_LIGHT0, GL_DIFFUSE, lightZeroColor);
        glMaterialfv(GL_FRONT, GL_SPECULAR, mat_flash1);
        glMaterialfv(GL_FRONT, GL_SHININESS, mat_flash_shiny1);
        glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient1);
    }
    glMatrixMode(GL_MODELVIEW);
    //glTranslatef( 0.0, 0.0, 25.0 );

        glColor3f(0,0,1);
    if( !arDebug ) glutSolidCone(22.0,50.0,30,1);
     else          glutWireCube(50.0);
    glDisable( GL_LIGHTING );

    glDisable( GL_DEPTH_TEST );

}


static void drawLimits( double trans1[3][4], int mode )
{
    double    gl_para[16];
    GLfloat   mat_ambient[]     = {0.0, 0.0, 1.0, 1.0};
    GLfloat   mat_ambient1[]    = {1.0, 0.0, 0.0, 1.0};
    GLfloat   mat_flash[]       = {0.0, 0.0, 1.0, 1.0};
    GLfloat   mat_flash1[]      = {1.0, 0.0, 0.0, 1.0};
    GLfloat   mat_flash_shiny[] = {50.0};
    GLfloat   mat_flash_shiny1[]= {50.0};
    GLfloat   light_position[]  = {100.0,-200.0,200.0,0.0};
    GLfloat   ambi[]            = {0.1, 0.1, 0.1, 0.1};
    GLfloat   lightZeroColor[]  = {0.9, 0.9, 0.9, 0.1};

    argDrawMode3D();
    argDraw3dCamera( 0, 0 );
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    /* load the camera transformation matrix */
    glMatrixMode(GL_MODELVIEW);
    argConvGlpara(trans1, gl_para);
    glLoadMatrixd( gl_para );

    if( mode == 0 ) {
        glEnable(GL_LIGHTING);
        glEnable(GL_LIGHT0);
        glLightfv(GL_LIGHT0, GL_POSITION, light_position);
        glLightfv(GL_LIGHT0, GL_AMBIENT, ambi);
        glLightfv(GL_LIGHT0, GL_DIFFUSE, lightZeroColor);
        glMaterialfv(GL_FRONT, GL_SPECULAR, mat_flash);
        glMaterialfv(GL_FRONT, GL_SHININESS, mat_flash_shiny);
        glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient);
    }
    else {
        glEnable(GL_LIGHTING);
        glEnable(GL_LIGHT0);
        glLightfv(GL_LIGHT0, GL_POSITION, light_position);
        glLightfv(GL_LIGHT0, GL_AMBIENT, ambi);
        glLightfv(GL_LIGHT0, GL_DIFFUSE, lightZeroColor);
        glMaterialfv(GL_FRONT, GL_SPECULAR, mat_flash1);
        glMaterialfv(GL_FRONT, GL_SHININESS, mat_flash_shiny1);
        glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient1);
    }
    glMatrixMode(GL_MODELVIEW);

    if( !arDebug ) glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
    //parede esquerda
    glPushMatrix();
        glTranslatef(-0.1,-160,127.5);
        glScalef(0.1,-310,250);
        if( !arDebug ) glutSolidCube(1);
        else glutWireCube(1);
    glPopMatrix();
    //chao
    glPushMatrix();
        glTranslatef(210,-160,-0.1);
        glScalef(430,-315,0.1);
        if( !arDebug ) glutSolidCube(1);
        else glutWireCube(1);
    glPopMatrix();
    //parede direita
    glPushMatrix();
        glTranslatef(421,-160,127.5);
        glScalef(0.1,-310,250);
        if( !arDebug ) glutSolidCube(1);
        else glutWireCube(1);
    glPopMatrix();
    //janela

    //baixo
    glPushMatrix();
        glTranslatef(210,0,47);
        glScalef(440,0.1,100);
        if( !arDebug ) glutSolidCube(1);
        else glutWireCube(1);
    glPopMatrix();
    //cima
    glPushMatrix();
        glTranslatef(210,0,200);
        glScalef(440,0.1,70);
        if( !arDebug ) glutSolidCube(1);
        else glutWireCube(1);
    glPopMatrix();
    //esquerda
    glPushMatrix();
        glTranslatef(52.5,0,120);
        glScalef(120,0.1,150);
        if( !arDebug ) glutSolidCube(1);
        else glutWireCube(1);
    glPopMatrix();
    //direita
    glPushMatrix();
        glTranslatef(370.5,0,120);
        glScalef(150,0.1,150);
        if( !arDebug ) glutSolidCube(1);
        else glutWireCube(1);
    glPopMatrix();

    glColorMask(GL_TRUE,GL_TRUE,GL_TRUE,GL_TRUE);

    // Desenha alguns cones na regi�o externa da caixa
    glEnable(GL_COLOR_MATERIAL);
    glColor3f(0,1,0);
    glPushMatrix();
        glTranslatef(400,50,0);
        glutSolidCone(50, 500,50,1);
    glPopMatrix();
    glPushMatrix();
        glTranslatef(130,100,0);
        glutSolidCone(50,300,50,1);
    glPopMatrix();
    glPushMatrix();
        glTranslatef(230,200,0);
        glutSolidCone(50,200,50,1);
    glPopMatrix();

    glPushMatrix();
        glTranslatef(-230,-200,0);
        glutSolidCone(50,300,50,1);
    glPopMatrix();
    glPushMatrix();
        glTranslatef(630,- 200,0);
        glutSolidCone(50,400,50,1);
    glPopMatrix();

    int i = 0;
    for (i = 0; i < decoCount; i++){
        drawDecoration(config->trans, itens[i], 0);
    }

    glDisable( GL_LIGHTING );

    glDisable( GL_DEPTH_TEST );

}

static void drawDecoration( double trans1[3][4], Deco item, int mode ){
    glColor3f(0,0,1);

    glPushMatrix();
        glTranslatef(item.pos_x, item.pos_y, item.pos_z);
        if (item.tipo == 1){ //mesa
           drawTable();
        } else if (item.tipo == 2){ // cadeira
            drawChair();
        } else if (item.tipo == 3){ // quadro
            drawPicture();
        }
    glPopMatrix();
}

static void drawTable(){
    glTranslatef(0,0,95);
    glScalef(200, 100, 1);
    glutSolidCube(1);
    glTranslatef(0,0,-50);
    glScalef(1, 1, 100 );
    glutWireCube(1);
}
static void drawChair(){
    glutSolidCone(15, 60, 20, 1);
    glTranslated(0,0,60);
    glScalef(25,25,5 );
    glutSolidSphere(1,50,50);
}
static void drawPicture(){
    glTranslatef(2,0,0);
    glScalef(5, 100, 50 );
    glutSolidCube(1);
}

static void undo(){
    printf("Called UNDO for type %d \n", undoAction.type );
    if (undoAction.type == 1){ //undo move
        itens[moving].pos_x = undoAction.orig_pos_x;
        itens[moving].pos_y = undoAction.orig_pos_y;
        itens[moving].pos_z = undoAction.orig_pos_z;
    } else if (undoAction.type == 2 && decoCount > 0){ // undo creation
        decoCount--;
        undoAction.type = 0;
    }
}
